import React from 'react';
import {View} from 'react-native';
import {
  Octicons,
  MaterialCommunityIcons,
  AntDesign,
  Feather,
} from '@common/icon';
import Color from '@common/color';
import Home from '@screens/home/home';
import {homeTab} from '@common/navigator';
import Booking from '@screens/booking/booking';
import Profile from '@screens/profile/profile';
import {fontRoboto, sizes} from '@common/fonts';
import HomeIcons from 'react-native-vector-icons/Octicons';
import ProfileIcons from 'react-native-vector-icons/Feather';
import Notification from '@screens/notification/notification';
import NotificationIcons from 'react-native-vector-icons/AntDesign';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import BookingIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const HomeTabs = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: Color.colorApp.purple,
        tabBarInactiveTintColor: Color.colorApp.ghostBLACK,
        tabBarBackground: () => (
          <View
            style={{
              flex: 1,
              backgroundColor: Color.colorApp.white,
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
            }}
          />
        ),
        tabBarStyle: {
          position: 'absolute',
          height: 70,
          borderRadius: 25,
          borderTopWidth: 0,
          shadowColor: Color.colorApp.purple,
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowRadius: 5,
          shadowOpacity: 1.0,
          elevation: 10,
          paddingBottom: 10,
          paddingTop: 10,
        },
        tabBarHideOnKeyboard: true,
        tabBarLabelStyle: {
          fontSize: sizes.base,
          fontFamily: fontRoboto.regular,
        },
      }}>
      <Tab.Screen
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <HomeIcons name={Octicons.home} color={color} size={30} />
          ),
          headerShown: false,
        }}
        name={homeTab.home}
        component={Home}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Booking',
          tabBarIcon: ({color, size}) => (
            <BookingIcons
              name={MaterialCommunityIcons.booking}
              color={color}
              size={30}
            />
          ),
          headerShown: false,
        }}
        name={homeTab.booking}
        component={Booking}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Notification',
          tabBarIcon: ({color, size}) => (
            <NotificationIcons
              name={AntDesign.notification}
              color={color}
              size={30}
            />
          ),
          headerShown: false,
        }}
        name={homeTab.notification}
        component={Notification}
      />
      <Tab.Screen
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <ProfileIcons name={Feather.profile} color={color} size={30} />
          ),
          headerShown: false,
        }}
        name={homeTab.profile}
        component={Profile}
      />
    </Tab.Navigator>
  );
};

export default HomeTabs;
