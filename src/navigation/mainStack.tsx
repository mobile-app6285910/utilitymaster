import React from 'react';
import HomeTabs from './homeTabs';
import {mainStack} from '@common/navigator';
import {createStackNavigator} from '@react-navigation/stack';

export type RootStackParamList = {
  homeTab: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

const MainStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{gestureEnabled: false}}
      initialRouteName={mainStack.homeTab}>
      <Stack.Screen
        options={{headerShown: false}}
        name={mainStack.homeTab}
        component={HomeTabs}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
