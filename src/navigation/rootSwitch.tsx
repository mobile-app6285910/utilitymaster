import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import MainStack from './mainStack';
import {rootSwitch} from '@common/navigator';
import {Platform, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import OnbroadingStack from '@screens/onboarding/onboarding';

const Stack = createStackNavigator();

const RootStack = () => {
  return (
    <SafeAreaProvider>
      <StatusBar barStyle="dark-content" backgroundColor="transparent" />
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            cardStyleInterpolator: Platform.select({
              android: CardStyleInterpolators.forFadeFromBottomAndroid,
              ios: CardStyleInterpolators.forHorizontalIOS,
            }),
          }}>
          <Stack.Screen name={rootSwitch.onboard} component={OnbroadingStack} />
          <Stack.Screen name={rootSwitch.main} component={MainStack} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default RootStack;
