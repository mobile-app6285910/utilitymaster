enum colorApp {
  purple = '#5D00FF',
  red = '#ff0000',
  yellow = '#ffff00',
  grey = '#CFCFCF',
  blue = '#6495ed',
  green = '#008000',
  black = '#000000',
  white = '#fff',
  ghostWHITE = '#f8f8ff',
  lightGREY = '#d3d3d3',
  orange = '#ffa500',
  opacityWHITE = 'rgba(255, 255, 255, 0.2)',
  opacityBLACK = 'rgba(0, 0, 0, 0.5)',
  CRIMSON = '#DC143C',
  ghostBLACK = '#303030',
}

export default {colorApp};
