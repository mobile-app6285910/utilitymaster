export type MainStackProps = {
  homeTab: string | any;
  editProfile: string;
  myRewards: string;
  favoriteTaskers: string;
  language: string;
  theme: string;
  help: string;
};

export const homeTab = {
  profile: 'ProfileStack',
  home: 'HomeStack',
  booking: 'BookingStack',
  notification: 'NotificationStack',
};

export const mainStack: MainStackProps = {
  homeTab: 'HomeTab',
  editProfile: 'EditProfileScreen',
  myRewards: 'MyRewardsScreen',
  favoriteTaskers: 'FavoriteTaskersScreen',
  language: 'LanguageScreen',
  theme: 'ThemeScreen',
  help: 'HelpScreen',
};

export const rootSwitch = {
  main: 'MainStack',
  onboard: 'OnboardingStack',
};
