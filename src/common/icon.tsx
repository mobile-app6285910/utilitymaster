const Octicons = {
  home: 'home',
};

const MaterialCommunityIcons = {
  booking: 'clipboard-text-multiple-outline',
  account: 'account-box-outline',
  gift: 'gift-outline',
  heart: 'cards-heart-outline',
  earth: 'earth',
  flare: 'flare',
  help: 'help-circle-outline',
  logout: 'logout',
};

const AntDesign = {
  notification: 'bells',
  arrowRight: 'right',
};

const Feather = {
  profile: 'user',
};

export {Octicons, MaterialCommunityIcons, AntDesign, Feather};
