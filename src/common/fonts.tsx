import {RFPercentage} from 'react-native-responsive-fontsize';
import {Dimensions} from 'react-native';

export const fontFamilies = {
  regular: 'AirbnbCereal_W_Lt',
  medium: 'AirbnbCereal_W_Md',
  semiBold: 'AirbnbCereal_W_Bd',
  bold: 'AirbnbCereal_W_XBd',
};

export const fontPatuaOne = {
  regular: 'PatuaOne-Regular',
};

export const fontRoboto = {
  black: 'Roboto-Black',
  blackItalic: 'Roboto-BlackItalic',
  bold: 'Roboto-Bold',
  boldItalic: 'Roboto-BoldItalic',
  italic: 'Roboto-Italic',
  light: 'Roboto-Light',
  lightItalic: 'Roboto-LightItalic',
  medium: 'Roboto-Medium',
  mediumItalic: 'Roboto-MediumItalic',
  regular: 'Roboto-Regular',
  thin: 'Roboto-Thin',
  thinItalic: 'Roboto-ThinItalic',
};

export const sizes = {
  base: 14,
  h1: RFPercentage(3.5),
  h2: RFPercentage(3.2),
  h3: RFPercentage(2.8),
  h4: 16,
  h5: 14,
  h6: 12,
  WIDTH: Dimensions.get('window').width,
  HEIGHT: Dimensions.get('window').height,
};
