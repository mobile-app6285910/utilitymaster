import React from 'react';
import Color from '@common/color';
import {StyleSheet, View} from 'react-native';
import UtilityList from './components/utilityList';
import HeaderProfile from './components/headerProfile';

const Profile = () => {
  return (
    <View style={styles.container}>
      <View style={styles.boxMargin}>
        <HeaderProfile />
        <UtilityList />
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.colorApp.white,
  },
  boxMargin: {
    marginHorizontal: 10,
  },
});
