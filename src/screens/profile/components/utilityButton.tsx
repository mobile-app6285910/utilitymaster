import React from 'react';
import Color from '@common/color';
import {fontRoboto, sizes} from '@common/fonts';
import IconRight from 'react-native-vector-icons/AntDesign';
import IconLeft from 'react-native-vector-icons/MaterialCommunityIcons';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

interface UtlButtonProps {
  nameIconLeft: string;
  nameIconRight: string;
  colorIcon?: string;
  title?: string;
  onPress?: () => void;
}

const UtilityButton = ({
  nameIconLeft,
  nameIconRight,
  colorIcon,
  title,
  onPress,
}: UtlButtonProps) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <IconLeft name={nameIconLeft} color={colorIcon} size={30} />
        <View style={styles.itemCenterRight}>
          <Text style={styles.txtTitle}>{title}</Text>
          <IconRight name={nameIconRight} color={colorIcon} size={20} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default UtilityButton;

const styles = StyleSheet.create({
  txtTitle: {
    marginLeft: 10,
    color: Color.colorApp.black,
    fontSize: sizes.h4,
    fontFamily: fontRoboto.regular,
  },
  container: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  itemCenterRight: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
});
