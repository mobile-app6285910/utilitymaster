import React from 'react';
import Color from '@common/color';
import UtilityButton from './utilityButton';
import {Divider} from 'react-native-elements';
import {StyleSheet, View} from 'react-native';
import {AntDesign, MaterialCommunityIcons} from '@common/icon';

const UtilityList = () => {
  return (
    <View style={styles.container}>
      <UtilityButton
        title={'Edit Profile'}
        nameIconLeft={MaterialCommunityIcons.account}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
      <UtilityButton
        title={'My Rewards'}
        nameIconLeft={MaterialCommunityIcons.gift}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
      <UtilityButton
        title={'Favorite Taskers'}
        nameIconLeft={MaterialCommunityIcons.heart}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
      <UtilityButton
        title={'Language'}
        nameIconLeft={MaterialCommunityIcons.earth}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
      <UtilityButton
        title={'Dark Mode'}
        nameIconLeft={MaterialCommunityIcons.flare}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
      <Divider width={2} color={Color.colorApp.ghostWHITE} />
      <UtilityButton
        title={'Help'}
        nameIconLeft={MaterialCommunityIcons.help}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
      <UtilityButton
        title={'Logout'}
        nameIconLeft={MaterialCommunityIcons.logout}
        nameIconRight={AntDesign.arrowRight}
        colorIcon={Color.colorApp.black}
      />
    </View>
  );
};

export default UtilityList;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
});
