import React from 'react';
import Color from '@common/color';
import {Avatar} from '@rneui/themed';
import {StyleSheet, Text, View} from 'react-native';
import {fontRoboto, sizes} from '@common/fonts';

const HeaderProfile = () => {
  return (
    <View style={styles.boxProfile}>
      <Avatar
        size={90}
        rounded
        source={{
          uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTcZsL6PVn0SNiabAKz7js0QknS2ilJam19QQ&usqp=CAU',
        }}
        avatarStyle={{
          borderWidth: 2,
          borderColor: Color.colorApp.black,
        }}
      />
      <View style={styles.txtStyle}>
        <Text style={[styles.txtName, {color: Color.colorApp.black}]}>
          Ly Cao Thang
        </Text>
        <Text style={[styles.txtEmail, {color: Color.colorApp.black}]}>
          thangly2k1@gmail.com
        </Text>
      </View>
    </View>
  );
};

export default HeaderProfile;

const styles = StyleSheet.create({
  boxProfile: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  txtStyle: {
    marginTop: 10,
    flexDirection: 'column',
    alignItems: 'center',
  },
  txtName: {
    fontSize: sizes.h1,
    fontFamily: fontRoboto.bold,
  },
  txtEmail: {
    fontSize: sizes.h2,
    fontFamily: fontRoboto.light,
  },
});
