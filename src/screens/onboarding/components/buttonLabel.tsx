import React from 'react';
import Color from '@common/color';
import {fontRoboto, sizes} from '@common/fonts';
import {StyleSheet, Text, View} from 'react-native';

const ButtonLabel = (label: any) => {
  return (
    <View style={styles.container}>
      <Text style={styles.txtBtnLabel}>{label}</Text>
    </View>
  );
};

export default ButtonLabel;

const styles = StyleSheet.create({
  container: {
    padding: 12,
  },
  txtBtnLabel: {
    color: Color.colorApp.black,
    fontFamily: fontRoboto.bold,
    fontSize: sizes.h3,
  },
});
