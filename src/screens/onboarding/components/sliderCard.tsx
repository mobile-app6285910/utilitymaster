import React from 'react';
import Color from '@common/color';
import {fontRoboto, sizes} from '@common/fonts';
import {View, Text, Image, StyleSheet} from 'react-native';

interface SliderItemProps {
  title?: string;
  description?: string;
  image: string;
}

const SliderCard = ({item}: {item: SliderItemProps}) => {
  return (
    <View style={styles.container}>
      <Image
        source={{uri: item.image}}
        style={styles.imageSlider}
        resizeMode="contain"
      />
      <Text style={styles.txtTitle}>{item.title}</Text>
      <Text style={styles.txtDescription}>{item.description}</Text>
    </View>
  );
};

export default SliderCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingTop: 100,
    backgroundColor: Color.colorApp.white,
  },
  imageSlider: {
    width: sizes.WIDTH,
    height: sizes.HEIGHT / 2,
  },
  txtTitle: {
    fontFamily: fontRoboto.boldItalic,
    color: Color.colorApp.black,
    fontSize: sizes.h1,
  },
  txtDescription: {
    textAlign: 'center',
    paddingTop: 5,
    fontFamily: fontRoboto.regular,
    color: Color.colorApp.black,
    fontSize: sizes.h3,
  },
});
