const slidesOnboard = [
  {
    id: 1,
    title: 'Easy & Fast Service',
    description: 'Book your service at your convenient time and enjoy the hassle free process',
    image: 'https://cdn.dribbble.com/userupload/2936496/file/original-906574b9684446e0f745bbf265cc9e76.png?resize=1504x1128',
  },
  {
    id: 2,
    title: 'Safety and quality',
    description: 'The best results and your satisfaction is our top priority',
    image: 'https://cdn.dribbble.com/userupload/2936535/file/original-790c0cc9c547521f986de109e0a43729.png?resize=450x338&vertical=center',
  },
  {
    id: 3,
    title: 'The cheapest',
    description: `We provide professional service at a friendly price.Let's make awesome changes to your life`,
    image: 'https://cdn.dribbble.com/userupload/2936538/file/original-c56683c0ab1c4e6b1a8f3059a00a99e0.png?resize=700x525&vertical=center',
  },
];

export default slidesOnboard;
