import React from 'react';
import Color from '@common/color';
import {StyleSheet} from 'react-native';
import {rootSwitch} from '@common/navigator';
import SliderCard from './components/sliderCard';
import ButtonLabel from './components/buttonLabel';
import slidesOnboard from './components/slidesList';
import {useNavigation} from '@react-navigation/native';
import AppIntroSlider from 'react-native-app-intro-slider';

const OnbroadingStack = () => {
  const navigation: any = useNavigation();

  return (
    <AppIntroSlider
      data={slidesOnboard}
      renderItem={({item}) => <SliderCard item={item} />}
      activeDotStyle={styles.activeDotStyle}
      showSkipButton
      renderNextButton={() => ButtonLabel('Next')}
      renderSkipButton={() => ButtonLabel('Skip')}
      renderDoneButton={() => ButtonLabel('Done')}
      onDone={() => {
        navigation.replace(rootSwitch.main);
      }}
    />
  );
};

export default OnbroadingStack;

const styles = StyleSheet.create({
  activeDotStyle: {
    backgroundColor: Color.colorApp.purple,
    width: 30,
  },
});
