import React, {useEffect} from 'react';
import {Platform} from 'react-native';
import Router from './src/navigation/rootSwitch';
import SplashScreen from 'react-native-splash-screen';

const App: React.FC = (): JSX.Element => {
  useEffect(() => {
    if (Platform.OS === 'android') SplashScreen.hide();
  }, []);

  return <Router />;
};

export default App;
